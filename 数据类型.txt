﻿&&是智能的
&是全部都要判断



一.基本数据类型


byte short char 不能相互转化。
boolean 不能跟其它数据类型相互转化。
int long double float .byte short char
字节”是byte，“位”是bit ；
1 byte = 8 bit ；
char 在java中是2个字节。java采用unicode，2个字节（16位）来表示一个字符。
short 2个字节
int float long 4个字节
double 8个字节
数据转换有两种转换。   1，从小范围转换成大的，直接转换。
                     2，从大范围转换成小的，要强制转换。截取低位。截断高位，保留低位





常见问题1：
    int money = 1000000000;//10亿

	int years = 20;

	int total = money*years;//返回的是负数

	long totall= money*years;  //返回的仍然是负数。默认是int，因此结果会转成int值，再转成long。但是已经发生了数据丢失

	long total2 = money*((long)years);   //先将一个因子变成long，整个表达式发生提升。全部用long来计算。
常见问题2：
     byte a=5;      //编译器优化，小于int 都会优化，只要不超过byte
     short b=7;
     short c=a+b;  //错误a+b为int类型。

一.引用数据类型
      接口
      类
      